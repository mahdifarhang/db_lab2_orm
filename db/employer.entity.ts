import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import ProjectEntity from './project.entity';

@Entity()
export default class EmployerEntity extends BaseEntity 
{
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  name: string;

  @Column()
  age: number

  @Column()
  budget: number

  // 1:n relation with projectEntity 
  @OneToMany( type => ProjectEntity , project => project.employer)
  project: EmployerEntity[];

}