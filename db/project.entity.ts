import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import EmployerEntity from './employer.entity';
import FreelancerEntity from './freelancer.entity';

@Entity()
export default class ProjectEntity extends BaseEntity 
{
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  name: string;

  @Column()
  price: number

  // 1:n relation with FreelancerEntity 
  @ManyToOne( type => FreelancerEntity , freelancer => freelancer.project)
  freelancer: FreelancerEntity[];

  // 1:n relation with EmployerEntity 
  @ManyToOne( type => EmployerEntity , employer => employer.project)
  employer: EmployerEntity[];

}