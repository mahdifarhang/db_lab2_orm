import { Injectable } from '@nestjs/common';
import EmployerEntity from '../../db/employer.entity';
import CreateEmployerDto from './dto/create-employer.dto';
import {getConnection} from "typeorm";

@Injectable()
export class EmployerServices {

  async insert(employerDetails: CreateEmployerDto): Promise<EmployerEntity> {
    const employerEntity: EmployerEntity = EmployerEntity.create();
    const {name } = employerDetails;
    employerEntity.name = name;
    await EmployerEntity.save(employerEntity);
    return employerEntity;
  }
  async getAllEmployers(): Promise<EmployerEntity[]> {
    return await EmployerEntity.find();
  }

  async getSingleEmployer(id: number): Promise<EmployerEntity> {
      return EmployerEntity.findOne(id);
  }

  async updateSingleEmployer(id: number): Promise<EmployerEntity> {
       emp: EmployerEntity = EmployerEntity.findOneOrFail(id);
       EmployerEntity.update(emp);
  }

  async deleteSingleEmployer(id: number) {
      EmployerEntity.delete(id);
  }
}