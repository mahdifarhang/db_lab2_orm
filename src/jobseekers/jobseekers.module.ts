import { Module } from '@nestjs/common';
import { EmployerController } from './jobseekers.controller';
import { JobseekersService } from './jobseekers.service';

@Module({
  controllers: [EmployerController],
  providers: [JobseekersService]
})
export class JobseekersModule {}
