import { Body, Controller, Get, ParseIntPipe, Post, Put, Delete } from '@nestjs/common';
import { EmployerServices } from './jobseekers.service';



import CreateEmployerDto from './dto/create-employer.dto';

@Controller('emplyer')
export class EmployerController {
  constructor(private readonly jobseekersService: EmployerServices) {}

//'postUser()' will handle the creating of new User
  @Post()
  createEmployer( @Body() employer: CreateEmployerDto) {
    return this.jobseekersService.insert(employer);
  }
// 'getAll()' returns the list of all the existing users in the database
  @Get()
  getAll() {
    return this.jobseekersService.getAllEmployers();
  }

//'getBooks()' return all the books which are associated with the user 
// provided through 'userID' by the request  
    @Get(`${id}`)
    getSingleEmployer() {
    return this.jobseekersService.getSingleEmployer(id);
    }

    @Put(`${id}`)
    updateSingleEmplyer( @Body() employer:CreateEmployerDto) {
        return this.jobseekersService.updateSingleEmployer(employer);
    }

    @Delete(`${id}`)
    deleteSingleEmployer() {
        this.jobseekersService.deleteSingleEmployer(id);
    }
}