import { Module } from '@nestjs/common';
import GenreServices from './genres.service';
import GenreController from './genres.controller';
@Module({
  imports: [],
  controllers: [GenreController],
  providers: [GenreServices],
})
export class GenreModule {}
//export default class GenreModule {}