import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HelloModule } from './hello/hello.module';
import { BooksModule } from './books/books.module';
import { UserModule } from './users/users.module';
import { GenreModule } from './genres/genres.module';
import { JobseekersModule } from './jobseekers/jobseekers.module';

@Module({
  imports: [HelloModule, BooksModule, UserModule, GenreModule, JobseekersModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
